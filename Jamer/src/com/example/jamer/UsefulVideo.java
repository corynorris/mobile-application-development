package com.example.jamer;

import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class UsefulVideo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video);
		String[] links = {
				"http://www.youtube.com/watch?v=PV1cpjdOGnQ"};
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, links);
		ListView listView = (ListView) findViewById(R.id.lvVidLinks);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				String url = ((TextView) view).getText().toString();

				// Implicit Intent to load the url
				Intent openUrl = new Intent(Intent.ACTION_VIEW);
				openUrl.setData(Uri.parse(url));
				startActivity(openUrl);
				
				// Make sure their is an intent to display (there should be,
				// since we'll create them
				PackageManager packageManager = getPackageManager();
				List<ResolveInfo> activities = packageManager
						.queryIntentActivities(openUrl, 0);
				boolean isIntentSafe = activities.size() > 0;

				// Display the intent, or throw an error
				if (isIntentSafe)
					startActivity(openUrl);
				else
					Toast.makeText(getBaseContext(), "Failed to open link",
							Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.video, menu);
		return true;
	}

}
