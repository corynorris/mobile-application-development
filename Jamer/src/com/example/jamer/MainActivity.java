package com.example.jamer;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	static public final String LEARNMORE_ACTION = "com.example.jamer";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void btnFindOutMore_click(View view) {
		// Make an intent and a chooser, to force the user to select which help
		// item
		// they'd like to see every time
		Intent intent = new Intent();
		intent.setAction(LEARNMORE_ACTION);
		Intent chooser = Intent.createChooser(intent, "Select an Item");

		// Make sure their is an intent to display (there should be, since we'll
		// create them
		PackageManager packageManager = getPackageManager();
		List<ResolveInfo> activities = packageManager.queryIntentActivities(
				intent, 0);
		boolean isIntentSafe = activities.size() > 0;

		// Display the intent, or throw an error
		if (isIntentSafe)
			startActivity(chooser);
		else
			Toast.makeText(getBaseContext(), "learnmore action not found",
					Toast.LENGTH_SHORT).show();

	}

	public void btnJam_click(View view) {
		Intent beginJaming = new Intent(this, Jam.class);
		try {
			startActivity(beginJaming);
		} catch (android.content.ActivityNotFoundException e) {
			Toast.makeText(getBaseContext(), "Action not found",
					Toast.LENGTH_SHORT).show();
		}
	}

}
