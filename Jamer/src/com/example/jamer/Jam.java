package com.example.jamer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Jam extends Activity {

	static final int SELECTTEXT_REQUEST = 0;
	
	private final HeadsetBroadcastReceiver myReceiver = new HeadsetBroadcastReceiver();
	
	boolean recording = false;
	MicPlayback m = null;
	int delay = 1;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jam);

		
		final Intent textSelector = new Intent(this, TextSelector.class);
		TextView tvReading = (TextView) findViewById(R.id.tvReading);
		tvReading.setOnLongClickListener(new OnLongClickListener() {
			
			public boolean onLongClick(View v) {
				
				
				startActivityForResult(textSelector, SELECTTEXT_REQUEST);

				return true;
			}

		});

		// Register the headset listener (so we can pause playback if it's
		// unplugged, stopping feedback)

		registerReceiver(myReceiver, new IntentFilter(
				Intent.ACTION_HEADSET_PLUG));

		SeekBar sbDelay = (SeekBar) findViewById(R.id.sbDelay);
		sbDelay.setMax(9);
		sbDelay.setProgress(0);
		sbDelay.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getBaseContext(), "Delay set to: " + delay,
						Toast.LENGTH_SHORT).show();

				// restart tracking if necessary
				if (recording) {

					m.close();
					try {
						m.join();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					m = new MicPlayback(delay);

				}
			}

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				delay = arg1 + 1;
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
			}

		});

	}

	@Override
	public void onPause() {
		super.onPause(); // Always call the superclass method first
		unregisterReceiver(myReceiver);
		if (recording) {
			m.close();
		}
	}

	@Override
	public void onResume() {
		super.onResume(); // Always call the superclass method first
		registerReceiver(myReceiver, new IntentFilter(
				Intent.ACTION_HEADSET_PLUG));
		if (recording) {
			m = new MicPlayback(delay);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.jam, menu);
		return true;
	}

	public void btnPlayback_click(View view) {
		Button p1_button = (Button) findViewById(R.id.btnPlayback);

		if (!recording) {
			m = new MicPlayback(delay);
			p1_button.setText("Stop");
		} else {
			m.close();
			p1_button.setText("Start");
		}
		recording = !recording;

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == SELECTTEXT_REQUEST) {

			if (resultCode == RESULT_OK) {
				TextView tvReading = (TextView) findViewById(R.id.tvReading);
				String result = data.getStringExtra("result");
				tvReading.setText(result);
			}
			if (resultCode == RESULT_CANCELED) {
				// Do Nothing
			}
		}
	}// onActivityResult

	@Override
	protected void onDestroy() {
	    super.onStop();  // Always call the superclass method first
		
	}
}
