package com.example.jamer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.RadioGroup;

public class TextSelector extends Activity {
		
	private final int	GOLDILOCKS = 0;
	private final int	REPUNZEL = 1;
	private final int	CINDERELLA =2;
	
	String[] items = { "In Southey\'s tale, three anthropomorphic bears � \"a Little, Small, Wee Bear, a Middle-sized Bear, and a Great, Huge Bear\" � live together in a house in the woods. Southey describes them as very good-natured, trusting, harmless, tidy, and hospitable. Each bear has his own porridge bowl, chair, and bed. One day they take a walk in the woods while their porridge cools. An old woman (who is described at various points in the story as impudent, bad, foul-mouthed, ugly, dirty and a vagrant deserving of a stint in the House of Correction) discovers the bears\' dwelling. She looks through a window, peeps through the keyhole, and lifts the latch. Assured that no one is home, she walks in. The old woman eats the Wee Bear\'s porridge, then settles into his chair and breaks it. Prowling about, she finds the bears\' beds and falls asleep in Wee Bear\'s bed. The climax of the tale is reached when the bears return.",
	"A couple, who want a child, live next to a walled garden belonging to an enchantress. The wife, experiencing the cravings associated with the arrival of her long-awaited pregnancy, notices a rapunzel plant (or, in some versions[7] of the story, rampion), growing in the garden and longs for it, desperate to the point of death. On each of two nights, the husband breaks into the garden to gather some for her; on a third night, as he scales the wall to return home, the enchantress, Dame Gothel, catches him and accuses him of theft. He begs for mercy, and the old woman agrees to be lenient, on condition that the then-unborn child be surrendered to her at birth. Desperate, the man agrees. When the baby girl is born, the enchantress takes the child to raise as her own, and names the baby Rapunzel. Rapunzel grows up to be the most beautiful child in the world with long golden hair.",
	"A widowed prince has a daughter, Zezolla (the Cinderella figure), who is tended by a beloved governess. The governess, with Zezolla's help, persuades the prince to marry her. The governess then brings forward six daughters of her own, who abuse Zezolla, and send her into the kitchen to work as a servant. The prince goes into the island of Sardinia, meets a fairy who gives presents to his daughter, and brings back for her, a golden spade, a golden bucket, a silken napkin, and a date seedling. The girl cultivates the tree, and when the king gives a ball, Zezolla appears dressed richly by a fairy living in the date tree. The king falls in love with her, but Zezolla runs away before he can find out who she is. Twice Zezolla escapes the king and his servants. The third time, the king's servant captures one of her slippers"		};
	
	private String selectedText = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text_selector);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.set_text, menu);
		return true;
	}

	public void btnSelect_click(View view) {
		Intent returnIntent = new Intent();
		
		RadioGroup rdbTextSelection = (RadioGroup)findViewById(R.id.rdbTextItems);
	    
	    // Check which radio button was clicked
	    switch(rdbTextSelection.getCheckedRadioButtonId()) {
	        case R.id.rdCinderella:
	        	selectedText = items[CINDERELLA];
	            break;
	        case R.id.rdGoldilocks:
	        	selectedText = items[GOLDILOCKS];
	            break;
	        case R.id.rdRepunzel:
	        	selectedText = items[REPUNZEL];
	            break;
	    }
		returnIntent.putExtra("result", selectedText);
		setResult(RESULT_OK, returnIntent);
		finish();
	}

}
