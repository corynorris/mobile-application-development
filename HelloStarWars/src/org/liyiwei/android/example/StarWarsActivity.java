package org.liyiwei.android.example;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import org.liyiwei.android.example.hellostarwars.R;

public class StarWarsActivity extends Activity
{
	static public final String STARWARS_ACTION = "org.liyiwei.android.example.starwars";
	static public final String JEDI_ACTION = "org.liyiwei.android.example.jedi";
	static public final String SITH_ACTION = "org.liyiwei.android.example.sith";	 
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // starwars button
        View.OnClickListener starwars_button_on_click_listener =
        		new View.OnClickListener()
        {
        	public void onClick(View v) 
        	{
        		Intent intent = new Intent();
        		intent.setAction(STARWARS_ACTION);
        		try
        		{
        			startActivity(intent);
        		}
				catch(android.content.ActivityNotFoundException e)
				{
					Toast.makeText(getBaseContext(), "starwars action not found", Toast.LENGTH_SHORT).show();
				}
        	}
        };

        Button starwars_button = (Button) findViewById(R.id.starwars_button);
        starwars_button.setOnClickListener(starwars_button_on_click_listener);
        
        // jedi button
        View.OnClickListener jedi_button_on_click_listener =
        		new View.OnClickListener()
        {
        	public void onClick(View v) 
        	{
        		Intent intent = new Intent();
        		intent.setAction(JEDI_ACTION);
        		try
        		{
        			startActivity(intent);
        		}
				catch(android.content.ActivityNotFoundException e)
				{
					Toast.makeText(getBaseContext(), "jedi action not found", Toast.LENGTH_SHORT).show();
				}
        	}
        };

        Button jedi_button = (Button) findViewById(R.id.jedi_button);
        jedi_button.setOnClickListener(jedi_button_on_click_listener);
        
        // sith button
        View.OnClickListener sith_button_on_click_listener =
        		new View.OnClickListener()
        {
        	public void onClick(View v) 
        	{
        		Intent intent = new Intent();
        		intent.setAction(SITH_ACTION);
        		try
        		{
        			startActivity(intent);
        		}
				catch(android.content.ActivityNotFoundException e)
				{
					Toast.makeText(getBaseContext(), "sith action not found", Toast.LENGTH_SHORT).show();
				}
        	}
        };

        Button sith_button = (Button) findViewById(R.id.sith_button);
        sith_button.setOnClickListener(sith_button_on_click_listener);
    }
}
