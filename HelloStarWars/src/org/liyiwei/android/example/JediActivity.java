package org.liyiwei.android.example;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.liyiwei.android.example.hellostarwars.R;

public class JediActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jedi);

        RadioGroup.OnCheckedChangeListener jedi_listener = 
        		new RadioGroup.OnCheckedChangeListener()
        {
        	public void onCheckedChanged(RadioGroup group, int checkedId) 
        	{
        		// customize your behavior here!
        		RadioButton jedi_button = (RadioButton) findViewById(checkedId);
        		Toast.makeText(getBaseContext(), jedi_button.getText() + " selected!", Toast.LENGTH_SHORT).show();
        	}
        };

        RadioGroup jedi_radio_group = (RadioGroup) findViewById(R.id.jedi_radio_group);
        jedi_radio_group.setOnCheckedChangeListener(jedi_listener);
    }
}
