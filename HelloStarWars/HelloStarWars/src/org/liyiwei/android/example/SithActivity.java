package org.liyiwei.android.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.liyiwei.android.example.hellostarwars.R;

public class SithActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sith);
        
        final ToggleButton sith_button = (ToggleButton) findViewById(R.id.sith_toggle_button);
        /*
        sith_button.setTextOn("Darth Sidious");
        sith_button.setTextOff("Palpatine");
        */

        ToggleButton.OnClickListener click_listener = new
        		ToggleButton.OnClickListener()
        {
			public void onClick(View v)
			{
				Toast.makeText(getBaseContext(), sith_button.isChecked() ? "Sith lord" : "Emperor", Toast.LENGTH_SHORT).show();
			}
        };
        
        sith_button.setOnClickListener(click_listener);
    }
}
